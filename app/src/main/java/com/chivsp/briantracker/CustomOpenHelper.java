package com.chivsp.briantracker;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CustomOpenHelper extends SQLiteOpenHelper {


    // データベースのバージョン(2,3と挙げていくとonUpgradeメソッドが実行される)
    static final private int VERSION = 1;

    // コンストラクタ　以下のように呼ぶこと
    public CustomOpenHelper(Context context){
        super(context, "Briantracker_DB", null, VERSION);
    }

    // データベースが作成された時に実行される処理
    @Override
    public void onCreate(SQLiteDatabase db) {
        /*
          テーブル新規作成
         */
        db.execSQL("CREATE TABLE PLAYER_INFO ( " +
                "PLAYER_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "PLAYER_NAME TEXT , " +
                "HAND_NUM INTERGER, " +
                "ACTION_NUM INTERGER, " +
                "RAISE_NUM INTERGER, " +
                "THREE_BET_NUM INTERGER, " +
                "FOUR_BET_NUM INTERGER, " +
                "CB_NUM INTERGER)");
        Log.d("debug", "onCreate(SQLiteDatabase db)");
    }

    // データベースをバージョンアップした時に実行される処理
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // アップデートの判別、古いバージョンは削除して新規作成
        db.execSQL(
                "DROP TABLE IF EXISTS PLAYER_INFO"
        );
        onCreate(db);
        Log.d("debug", "onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)");
    }
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
        Log.d("debug", "onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion)");
    }

    // データベースが開かれた時に実行される処理
    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        Log.d("debug", "onOpen(SQLiteDatabase db)");
    }


}