package com.chivsp.briantracker;

import android.database.Cursor;

public class PlayerData {
    /** プレイヤー名 */
    private String playerName ="";
    /** ハンド数 */
    private int handNum = 0;
    /** 参加数 */
    private int actionNum = 0;
    /** レイズ数 */
    private int raiseNum = 0;
    /** ３bet数 */
    private int threeBetNum = 0;
    /** ４bet+数 */
    private int forBetPlusNum = 0;
    /** コンテニューションbet数 */
    private int cBetNum = 0;

    public PlayerData(Cursor c) {
        this.playerName = c.getString(1);
        this.handNum = c.getInt(2);
        this.actionNum = c.getInt(3);
        this.raiseNum = c.getInt(4);
        this.threeBetNum = c.getInt(5);
        this.forBetPlusNum = c.getInt(6);
        this.cBetNum = c.getInt(7);
    }

    public String getPlayerName() {
        return playerName;
    }

    public int getHandNum() {
        return handNum;
    }

    public int getActionNum() {
        return actionNum;
    }

    public int getRaiseNum() {
        return raiseNum;
    }

    public int getThreeBetNum() {
        return threeBetNum;
    }

    public int getForBetPlusNum() {
        return forBetPlusNum;
    }

    public int getcBetNum() {
        return cBetNum;
    }
}